using System;
using System.Collections.Generic;

namespace SEPCw1.Models
{
    public class Showing
    {
        public long Id { get; set; }
        public DateTime TimeOfShowing { get; set; }

        public string MovieId { get; set; }
        public Movie Movie { get; set; }

        /// <summary>
        /// Foreign Key to the Screen ID
        /// </summary>
        public long ScreenId { get; set; }
        /// <summary>
        /// A reference to the screen (for programmer use)
        /// </summary>
        public Screen Screen { get; set; }

        /// <summary>
        /// The price (in pence).
        /// For example: 46 would be £0.46, 512 would be £5.12
        /// </summary>
        public uint Price { get; set; }

        /// <summary>
        /// Multiplier applied for luxury seats.
        /// For example 1.1 means 10% higher cost than standard.
        /// Using float instead of double to save memory
        /// </summary>
        public float LuxuryMultiplier { get; set; }

        /// <summary>
        /// Multiplier applied for discount seats or bookings
        /// 0.9 means 10% discount. 1.0 means no discount applicable
        /// </summary>
        public float DiscountMultiplier { get; set; }

        public List<Booking> Bookings { get; set; }
    }
}
