using System.ComponentModel.DataAnnotations;

namespace SEPCw1.Models
{
    public class PaymentRequestModel
    {
        [Required]
        public int ShowingId { get; set; }

        [Required]
        public int AdultSeats { get; set; }
        [Required]
        public int ChildSeats { get; set; }
        [Required]
        public int SeniorSeats { get; set; }
        [Required]
        public int VipSeats { get; set; }
    }
}
