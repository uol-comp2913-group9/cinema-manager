using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace SEPCw1.Models
{
    /// <summary>
    /// A composite relation for storing information about bookings
    /// </summary>
    public class Booking
    {
        public long Id { get; set; }

        /// <summary>
        /// Foreign key for the Account
        /// </summary>
        public long AccountId { get; set; }
        /// <summary>
        /// Reference to the account for programmer use
        /// </summary>
        public Account Account { get; set; }

        /// <summary>
        /// Foreign key for the Showing
        /// </summary>
        public long ShowingId { get; set; }
        /// <summary>
        /// Reference to the showing for programmer use
        /// </summary>
        public Showing Showing { get; set; }

        /// <summary>
        /// Foreign key for Seat
        /// </summary>
        public long SeatId { get; set; }
        /// <summary>
        /// Reference to the seat for programmer
        /// </summary>
        public Seat Seat { get; set; }

        public List<Sale> Sale { get; set; }
    }

    public class MakeBookingRequest
    {
        [Required]
        public long ShowingID { get; set; }

        [Required]
        public List<long> Seats { get; set; }
    }

    public class SelectSeatRequest
    {
        [Required]
        public long ShowingID { get; set; }

        [Required]
        public long SeatID { get; set; }
    }
}
