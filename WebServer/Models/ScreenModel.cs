using System.Collections.Generic;

namespace SEPCw1.Models
{
    public class Screen
    {
        public long Id { get; set; }
        public int SeatCount { get; set; }

        /// <summary>
        /// Back-reference to the showings
        /// See ShowingModel.cs
        /// </summary>
        public List<Showing> Showings { get; set; }
    }
}
