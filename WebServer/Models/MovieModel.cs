using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SEPCw1.Models
{
    public enum Certificate
    {
        U = 0,
        PG = 1,
        PG_13 = 2,
        Rated_15 = 3,
        Rated_18 = 4

    }

    /// <summary>
    /// The Movie database model
    /// </summary>
    /// Primary Key:
    ///     MovieID
    /// 
    /// Its main purpose is to link a Ticket Price to the Movie's IMDB reference.
    /// In order to get the movie's Title, Description, Director etc, you should use IMDBService
    public class Movie
    {
        // Utilises IMDB's external API for getting title details
        public string MovieID { get; set; }
        // Price of ticket (in GBpence). For example: £1.49 is represented as 149
        public int TicketPrice { get; set; }

        public Certificate Certificate { get; set; }

        public List<Showing> Showings { get; set; }

        public string PosterLink { get; set; }

        public string MovieName { get; set; }
    }

    public class AddMovieRequest
    {
        [Required]
        public string MovieID { get; set; }
        [Required]
        public string PosterLink { get; set; }
        [Required]
        public Certificate Certificate { get; set; }
        [Required]
        public int TicketPrice { get; set; }
    }
}
