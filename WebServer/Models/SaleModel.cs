using System;

namespace SEPCw1.Models
{
    public class Sale
    {
        public long SaleId { get; set; }

        /// <summary>
        /// Foreign key for the bookings relation
        /// </summary>
        public long BookingId { get; set; }
        /// <summary>
        /// Reference to the booking used by this sale
        /// </summary>
        public Booking Booking { get; set; }

        public DateTime TimeOfSale { get; set; }
    }
}
