using Microsoft.EntityFrameworkCore;


namespace SEPCw1.Models
{
    public class CinemaContext : DbContext
    {
        public CinemaContext(DbContextOptions<CinemaContext> options)
            : base(options)
        {
        }

        public DbSet<Movie> Movies { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Sale> Sales { get; set; }
        public DbSet<Booking> Bookings { get; set; }
        public DbSet<Screen> Screens { get; set; }
        public DbSet<Seat> Seats { get; set; }
        public DbSet<SectionMovie> SectionMovies { get; set; }
        public DbSet<Showing> Showings { get; set; }
        public DbSet<CashPayment> CashPayments { get; set; }
    }
}
