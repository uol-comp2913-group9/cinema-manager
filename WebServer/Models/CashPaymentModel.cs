using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SEPCw1.Models
{

    public class CashPayment
    {
        public long Id { get; set; }
        public long AccountId { get; set; }
        public int ShowingId { get; set; }
        public int AdultSeats { get; set; }
        public int ChildSeats { get; set; }
        public int SeniorSeats { get; set; }
        public int VipSeats { get; set; }
        public long PricePaid {get;set;}
        public bool Paid { get; set; }
    }

}
