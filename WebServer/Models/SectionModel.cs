namespace SEPCw1.Models
{
    public enum SectionType
    {
        Trending = 0,
        ComingSoon = 1,
        NewRelease = 2
    }

    /// <summary>
    /// A composite table for Movies and Sections
    /// </summary>
    public class SectionMovie
    {
        public long Id { get; set; }

        public string MovieId { get; set; }
        public Movie Movie { get; set; }

        public SectionType SectionType { get; set; }
    }
}
