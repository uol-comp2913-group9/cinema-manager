using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SEPCw1.Models
{
    public class Account
    {
        public long Id { get; set; }
        public string Username { get; set; }
        public string PasswordHash { get; set; }
        public string PasswordSalt { get; set; }
        public bool IsDiscount { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public int Age { get; set; }
        public string Role { get; set; }

        [NotMapped]
        public string Token { get; set; }
    }
}
