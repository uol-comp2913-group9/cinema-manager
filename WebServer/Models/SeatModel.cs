namespace SEPCw1.Models
{
    public class Seat
    {
        public long Id { get; set; }

        /// <summary>
        /// Determines whether a more expensive seat price should be given
        /// </summary>
        public bool IsVIP { get; set; }

        /// <summary>
        /// Foreign key pointing to the Screen relation
        /// </summary>
        public long ScreenId { get; set; }
        /// <summary>
        /// Reference to the screen for programmer use
        /// </summary>
        public Screen Screen { get; set; }
    }
}
