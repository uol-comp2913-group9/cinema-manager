using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SEPCw1.Services;
using Stripe;
using SEPCw1.Models;
using Microsoft.AspNetCore.Authorization;

namespace SEPCw1.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class PaymentsController : Controller
    {
        private readonly ILogger<TestController> _logger;
        private readonly PaymentsService _paymentsService;
        private readonly BookingService _bookingService;

        public PaymentsController(ILogger<TestController> logger, PaymentsService paymentsService, BookingService bookingService)
        {
            _logger = logger;
            _paymentsService = paymentsService;
            _bookingService = bookingService;
        }

        [HttpPost("PayByCard")]
        public string PayByCard([FromBody] PaymentRequestModel model)
        {
            //Create a test charge and return session id.
            _logger.LogInformation("Payment get");
            Showing show = _bookingService.FindShowing(model.ShowingId);
            string sessionID = _paymentsService.CreateCharge(model,show.Price,show.DiscountMultiplier,show.LuxuryMultiplier);
            return sessionID;
        }

        [HttpPost("PayByCash")]
        public async Task<string> PayByCash([FromBody] PaymentRequestModel model)
        {
            var accountID = long.Parse(User.Identity.Name);
            Showing show = _bookingService.FindShowing(model.ShowingId);
            string refferenceCode = await _paymentsService.CreateCashCharge(model,show.Price,show.DiscountMultiplier,show.LuxuryMultiplier, accountID);
            return refferenceCode;
        }

        [HttpGet("GetCashPayment/{refCode}")]
        public async Task<CashPayment> GetCashPayment(long refCode)
        {
            try
            {
                return await _paymentsService.GetCashPaymentAsync(refCode);
            }
            catch (System.Exception)
            {
                return null;
            }
        }

        [HttpPost("AcceptCashPayment/{refCode}")]
        public async Task AcceptCashPayment(long refCode)
        {
            await _paymentsService.AcceptCashPayment(refCode);
        }

        [HttpPost]
        [Route("webhook")]
        public async Task<IActionResult> WebhookResponse()
        {
            try
            {
               string json = await new StreamReader(HttpContext.Request.Body).ReadToEndAsync();
                _logger.LogInformation("Got response about successfull payment from stripe.");
                _logger.LogInformation(json);

                //Now parse the webhook and add sale to the database, send tickets to email.
               return Ok();
            }
            catch (StripeException e)
            {
                return BadRequest();
            }
        }
    }
}
