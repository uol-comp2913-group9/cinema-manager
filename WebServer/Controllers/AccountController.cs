using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using SEPCw1.Models;
using SEPCw1.Services;

namespace SEPCw1.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly AccountService _service;

        public AccountController(AccountService service)
        {
            _service = service;
        }

        // GET: api/Account
        [HttpGet("")]
        public async Task<ActionResult<Account>> GetAccount()
        {
            long id = long.Parse(User.Identity.Name);
            Account account = await _service.GetAccount(id);
            return account;
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] RegisterModel model)
        {
            Account user;
            try
            {
                user = await _service.Register(model.Email, model.Password, model.Name, model.Surname, model.Age);    
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException ae)
                {
                    return BadRequest(new { message = ae.Message });
                }
                else
                {
                    return BadRequest("Unknown error");
                }
            }


            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("SEPcwGroupProjectTemporaryKey");
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    //Claim type Name is used because it is default claim type.
                    new Claim(ClaimTypes.Name, user.Id.ToString()),
                    new Claim(ClaimTypes.Role, user.Role),
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);

            // return basic user info and authentication token without password
            user.Token = tokenString;
            user.PasswordHash = null;
            user.PasswordSalt = null;

            return Ok(user);
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public async Task<IActionResult> Authenticate([FromBody] AuthenticateModel model)
        {
            Account user = await _service.Authenticate(model.Username, model.Password);
            if (user == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("SEPcwGroupProjectTemporaryKey");
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    //Claim type Name is used because it is default claim type.
                    new Claim(ClaimTypes.Name, user.Id.ToString()),
                    new Claim(ClaimTypes.Role, user.Role)
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);

            // return basic user info and authentication token without password
            user.Token = tokenString;
            user.PasswordHash = null;
            user.PasswordSalt = null;

            return Ok(user);
        }

        [HttpGet("Bookings/{accountID}")]
        public async Task<IActionResult> ViewMyBookings()
        {
            var accountID = long.Parse(User.Identity.Name);

            List<Booking> myBookings = await _service.ViewMyBookingsAsync(accountID);

            if (myBookings != null)
                return Ok(myBookings);
            else
                return BadRequest("Sorry, something went wrong");
        }
    }
}
