using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using SEPCw1.Models;
using SEPCw1.Services;

namespace SEPCw1.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]

    public class BookingController : ControllerBase
    {

        class MyBookingsResult{
            public string MovieName {get;set;}
            public long SeatId { get; internal set; }
            public long Screen { get; internal set; }
            public DateTime Date { get; internal set; }
            public long Id { get; internal set; }
        }

        private readonly BookingService _bookingService;
        private readonly MovieService _movieService;

        public BookingController(BookingService service,MovieService movieService)
        {
            _bookingService = service;
            _movieService=movieService;
        }

        [HttpPost("MakeBooking")]
        public async Task<IActionResult> MakeBooking([FromBody] MakeBookingRequest requestPayload)
        {
            if (requestPayload.ShowingID <= 0)
                return BadRequest("ShowingID is null or empty");

            var accountID = long.Parse(User.Identity.Name);
            List<Booking> bookings;

            try
            {
                bookings = await _bookingService.MakeBookingAsync(requestPayload.ShowingID, accountID, requestPayload.Seats);
            }
            catch (KeyNotFoundException ex)
            {
                return BadRequest(ex.Message);
            }

            if (bookings.Count != 0)
                return Ok();
            else
                return BadRequest("This booking could not be made");
        }

        [HttpPost("SelectSeat")]
        public async Task<IActionResult> SelectSeat([FromBody] SelectSeatRequest requestPayload)
        {
            if (requestPayload.SeatID <= 0)
                return BadRequest("seatID is null or empty");

            if (await _bookingService.SeatIsAvailableAsync(requestPayload.SeatID, requestPayload.ShowingID))
                return Ok("Seat is available");
            else
                return BadRequest("Sorry, that seat is booked");
        }

        [HttpGet("GetMyTickets")]
        public async Task<IActionResult> GetMyTickets()
        {

            var accountID = long.Parse(User.Identity.Name);

            List<Booking> myBookings = await _bookingService.GetMyBookings(accountID);
            if(myBookings.Count == 0){
                return Ok(myBookings);
            }

            List<MyBookingsResult> ret = new List<MyBookingsResult>();
            foreach (Booking booking in myBookings)
            {
                Showing show =  _bookingService.FindShowing(booking.ShowingId);
                Movie mv =  await _movieService.GetMovieAsync(show.MovieId);
                MyBookingsResult result = new MyBookingsResult();
                result.MovieName = mv.MovieName;
                result.SeatId = booking.SeatId;
                result.Screen = show.ScreenId;
                result.Date = show.TimeOfShowing;
                result.Id = booking.Id;
                ret.Add(result);
            }

            return Ok(ret);
        }

        [AllowAnonymous]
        [HttpGet("GetTicketPdf/{bookingID}")]
        public async Task<FileStreamResult> GetTicketPdf(long bookingId)
        {

            //var accountID = long.Parse(User.Identity.Name);
            Booking booking = await _bookingService.GetBookingAsync(bookingId);
            Showing show =  _bookingService.FindShowing(booking.ShowingId);
            Movie mv =  await _movieService.GetMovieAsync(show.MovieId);
            
            MemoryStream ms = _bookingService.GetTicketPDFAsync(booking.Id,mv.MovieName,show.TimeOfShowing,booking.SeatId);
            FileStreamResult fileStreamResult = new FileStreamResult(ms, "application/pdf");
            fileStreamResult.FileDownloadName = "Ticket.pdf";
            return fileStreamResult;
        }

        [HttpGet("GetBookingPrice/{bookingID}")]
        public async Task<IActionResult> GetBookingPrice(Booking booking)
        {
            if (booking == null)
                return BadRequest("booking is null or empty");

            uint price = await _bookingService.GetBookingsPriceAsync(new List<long>() { booking.Id });

            if (price <= 0)
                return BadRequest("Could not get price");
            else
                return Ok(price);
        }

        [HttpGet("GetWeeklySales")]
        public async Task<IActionResult> GetWeeklySales()
        {
            List<BookingService.MovieAndGross> weeklySales;
            int all = 0;
            weeklySales = await _bookingService.GetSpecificSalesAsync(all);

            if (weeklySales.Count != 0)
                return Ok(weeklySales);
            else
                return BadRequest("Could not get Weekly Sales");
        }

        // Top grossing movies in a week
        [HttpGet("GetWeeklySales")]
        public async Task<IActionResult> GetTopGrossingMovies()
        {
            List<BookingService.MovieAndGross> topGrossingMovies;

            topGrossingMovies = await _bookingService.GetSpecificSalesAsync(10);

            if (topGrossingMovies.Count != 0)
                return Ok(topGrossingMovies);
            else
                return BadRequest("Could not get topGrossingMovies Sales");
        }

        // gets the available seats for a particular showing of a particular movie
        // GET: api/Booking/GetAvailableSeats/movie1/3
        [AllowAnonymous]
        [HttpGet("GetAvailableSeats/{movieID}/{showingID}")]
        public async Task<IActionResult> GetAvailableSeats(long showingID)
        {
            if (showingID <= 0)
                return BadRequest("ShowingID is null or empty");

            List<Seat> availableSeats = await _bookingService.GetAvailableSeatsAsync(showingID);

            if (availableSeats.Count != 0)
                return Ok(availableSeats);
            else
                return NotFound("Could not find any available seats for that show");
        }

        // gets the booked seats for a particular showing of a particular movie
        // GET: api/Booking/GetBookedSeats/movie1/3
        [HttpGet("GetBookedSeats/{movieID}/{showingID}")]
        [AllowAnonymous]
        public async Task<IActionResult> GetBookedSeats(long showingID)
        {
            if (showingID <= 0)
                return BadRequest("ShowingID is null or empty");

            List<Seat> bookedSeats = await _bookingService.GetBookedSeatsAsync(showingID);

            if (bookedSeats.Count != 0)
                return Ok(bookedSeats);
            else
                return NotFound("Could not find any booked seats for that show");
        }

    }

}
