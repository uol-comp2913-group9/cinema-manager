﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SEPCw1.Models;
using SEPCw1.Services;

namespace SEPCw1.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TestController : Controller
    {
        private readonly ILogger<TestController> _logger;
        private readonly IMDBService _imdbService;

        private readonly MovieService _movieService;

        public TestController(ILogger<TestController> logger, IMDBService imdbService, MovieService movieService)
        {
            _logger = logger;
            _imdbService = imdbService;
            _movieService = movieService;
        }

        // api/test/testGet
        [HttpGet("testGet")]
        public string GetHelloWorldString()
        {
            _logger.LogDebug("Sending hello world response");
            return "Hello world!";
        }

        [HttpGet("getMovieDesc/{id}")]
        public async Task<string> GetMovieDescription(string id)
            => await _imdbService.GetMovieDescriptionAsync(id);

        [HttpGet("getMovieTitle/{id}")]
        public async Task<string> GetMovieTitle(string id)
            => await _imdbService.GetMovieTitleAsync(id);

        [HttpGet("search/{name}")]
        public async Task<string> SearchForMovie(string name, int numResults)
            => JsonConvert.SerializeObject(await _imdbService.SearchMoviesByNameAsync(name, numResults));

        [HttpGet("testAdd")]
        public async Task<List<string>> AddMoviesToDB()
        {
            List<string> ids = new List<string>{"tt0068646", "tt0071562",
                "tt0468569",
                "tt0050083",
                "tt0108052",
                "tt0167260",
                "tt0110912",
                "tt0060196",
                "tt0120737",
                "tt0137523",
                "tt1375666",
                "tt0133093",
                "tt0047478",
                "tt0102926",
                "tt0317248",
                "tt0114369"
            };
            List<string> titles = new();
            foreach (var id in ids)
            {
                await _movieService.AddMovieAsync(id, Certificate.PG, 40,"");
                titles.Add(await _imdbService.GetMovieTitleAsync(id));
            }
            return titles;
        }

        [HttpGet("testAddScreens")]
        public async void AddScreensToDB() => await _movieService.AddScreensAsync();

        [HttpGet("testAddSeats")]
        public async void AddSeatsToDB() => await _movieService.AddSeatsAsync();

        [HttpGet("testAddShowings")]
        public async void AddShowingsToDB() => await _movieService.AddShowingsAsync();
    }
}
