using System.Collections.Generic;
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SEPCw1.Models;
using SEPCw1.Services;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;

namespace SEPCw1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MovieController : ControllerBase
    {
        private class RetrunShowing
        {
            public string Date { get; set; }
            public uint Price { get; set; }
            public long ID { get; set; }
            public long Screen {get; set;}
            public float LuxuryMultiplier {get;  set;}
            public float DiscountMultiplier {get;  set;}

            public RetrunShowing(Showing showing)
            {
                Date = showing.TimeOfShowing.ToString();
                Price = showing.Price;
                ID = showing.Id;
                Screen = showing.ScreenId;
                LuxuryMultiplier = showing.LuxuryMultiplier;
                DiscountMultiplier = showing.DiscountMultiplier;
            }

            public static List<RetrunShowing> RetrunShowings(List<Showing> showings)
            {
                List<RetrunShowing> ret = new List<RetrunShowing>();
                foreach (Showing show in showings)
                    ret.Add(new RetrunShowing(show));
                return ret;
            }

        }
        private struct ReturnMovie
        {
            public string movieID;
            public string title;
            public string description;
            public string director;
            public List<string> cast;
            public int ticketPrice;
            public List<RetrunShowing> showings;
            public Certificate certificate;
            public string posterLink;


            public ReturnMovie(string movieID, string title, string description, string director,
                List<string> cast, int ticketPrice, List<RetrunShowing> showings, Certificate certificate, string poster)
            {
                this.movieID = movieID;
                this.title = title;
                this.description = description;
                this.director = director;
                this.cast = cast;
                this.ticketPrice = ticketPrice;
                this.showings = showings;
                this.certificate = certificate;
                this.posterLink = poster;
            }

            public ReturnMovie(Movie movie, MovieService service, IMDBService iMDBService) : this(
                movie.MovieID,
                // I hear that using .Result is bad, but we can't async a constructor so it's necessary
                iMDBService.GetMovieTitleAsync(movie).Result,
                iMDBService.GetMovieDescriptionAsync(movie).Result,
                iMDBService.GetMovieDirectorNameAsync(movie.MovieID).Result,
                iMDBService.GetMovieCastAsync(movie.MovieID).Result,
                movie.TicketPrice,
                RetrunShowing.RetrunShowings(service.GetShowTimesAsync(movie.MovieID).Result.ToList()),
                movie.Certificate,
                movie.PosterLink
            )
            { }
        }

        private class HomePageMovies
        {
            public List<Movie> TodaysFilms { get; set; }
            public List<Movie> TrendingFilms { get; set; }
            public List<Movie> NewReleases { get; set; }
            public List<Movie> ComingSoon { get; set; }
        }

        private readonly MovieService _service;
        private readonly IMDBService _imdbservice;
        private readonly ILogger<TestController> _logger;

        public MovieController(ILogger<TestController> logger, MovieService service, IMDBService iMDBService)
        {
            _service = service;
            _imdbservice = iMDBService;
            _logger = logger;

        }

        [HttpPost("AddMovie")]
        public async Task<IActionResult> AddMovie([FromBody] AddMovieRequest movieRequest)
        {
            Movie movie;
            try
            {
                movie = await _service.AddMovieAsync(movieRequest.MovieID, movieRequest.Certificate, movieRequest.TicketPrice, movieRequest.PosterLink);
            }
            catch (DbUpdateException ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok(movie);
        }

        // GET: api/Movie/SearchByName/titanic 
        [HttpGet("SearchByName/{name}")]
        public async Task<IActionResult> SearchMovieByName(string name)
        {
            //Do parameter validation here
            if (name == null || name == "")
                return BadRequest("Name is null or empty");

            List<Tuple<string,string>> moviesFound = await _service.GetMoviesByNameAsync(name);

            //If movie was found, return OK with movie object, else return NotFound result.
            if (moviesFound.Count != 0)
                return Ok(moviesFound);
            else
                return NotFound("Could not find any movies by that name");
        }

        // GET: api/Movie/SearchByCast/DiCaprio
        [HttpGet("SearchByCast/{cast}")]
        public async Task<IActionResult> SearchMovieByCast(string cast)
        {
            //Do parameter validation here
            if (cast == null || cast == "")
                return BadRequest("Cast name(s) is null or empty");

            List<Movie> moviesFound = await _service.GetMoviesByCastAsync(cast);

            //If movie was found, return OK with movie object, else return NotFound result.
            if (moviesFound.Count != 0)
                return Ok(moviesFound);
            else
                return NotFound("Could not find any movies with those cast members");
        }

        // GET: api/Movie/GetMoviesBySection/trending 
        [HttpGet("GetMoviesBySection/{section}")]
        public async Task<IActionResult> GetMoviesBySection(SectionType section)
        {
            //Do parameter validation here
            if (section <= 0)
                return BadRequest("Section is null or empty");

            List<Movie> moviesFound = await _service.GetMoviesBySectionAsync(section);

            //If movie was found, return OK with movie object, else return NotFound result.
            if (moviesFound.Count != 0)
                return Ok(moviesFound);
            else
                return NotFound("Could not find any movies in that section");
        }

        // GET: api/Movie/GetMoviesBySection/comedy 
        [HttpGet("GetMoviesByGenre/{genre}")]
        public async Task<IActionResult> GetMoviesByGenre(string genre)
        {
            //Do parameter validation here
            if (genre == null || genre == "")
                return BadRequest("Genre is null or empty");


            List<Movie> moviesFound = await _service.GetMoviesByGenreAsync(genre);

            //If movie was found, return OK with movie object, else return NotFound result.
            if (moviesFound.Count != 0)
                return Ok(moviesFound);
            else
                return NotFound("Could not find any movies in that genre");
        }



        // GET: api/Movie/5
        [HttpGet("GetMovie/{id}")]
        public async Task<IActionResult> GetMovie(string id)
        {
            Movie movie = null;
            movie = await _service.GetMovieAsync(id);
            if (movie == null)
                return NotFound("Sorry could not find movie's info");
            return Ok(JsonConvert.SerializeObject(new ReturnMovie(movie, _service, _imdbservice)));
        }


        // GET: api/Movie/5
        [HttpGet("GetHomepageMovies")]
        public async Task<IActionResult> GetHomepageMovies()
        {
            HomePageMovies returnMovies = new HomePageMovies();
            returnMovies.TodaysFilms = await _service.GetTodaysFilms();
            returnMovies.TrendingFilms = await _service.GetTrendingFilms();
            returnMovies.NewReleases = await _service.GetNewReleases();
            returnMovies.ComingSoon = await _service.GetComingSoon();

            return Ok(returnMovies);
        }

        // GET: api/Movie/5
        [HttpGet("GetMovieNames")]
        public async Task<List<string>> GetMovieNames()
        {
            List<string> ret = new List<string>();
            ret.Add("Movie1");
            ret.Add("Movie2");
            ret.Add("Movie3");
            return ret;
        }
    }
}
