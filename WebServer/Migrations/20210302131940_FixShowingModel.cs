﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SEPCw1.Migrations
{
    public partial class FixShowingModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "MovieId",
                table: "Showings",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0L);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MovieId",
                table: "Showings");
        }
    }
}
