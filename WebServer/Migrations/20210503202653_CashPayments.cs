﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SEPCw1.Migrations
{
    public partial class CashPayments : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CashPayments",
                columns: table => new
                {
                    Id = table.Column<long>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    AccountId = table.Column<long>(type: "INTEGER", nullable: false),
                    ShowingId = table.Column<int>(type: "INTEGER", nullable: false),
                    AdultSeats = table.Column<int>(type: "INTEGER", nullable: false),
                    ChildSeats = table.Column<int>(type: "INTEGER", nullable: false),
                    SeniorSeats = table.Column<int>(type: "INTEGER", nullable: false),
                    VipSeats = table.Column<int>(type: "INTEGER", nullable: false),
                    PricePaid = table.Column<int>(type: "INTEGER", nullable: false),
                    Paid = table.Column<bool>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CashPayments", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CashPayments");
        }
    }
}
