﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SEPCw1.Migrations
{
    public partial class AccountIsDiscount : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "TimeOfSale",
                table: "Sales",
                type: "TEXT",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "IsDiscount",
                table: "Accounts",
                type: "INTEGER",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TimeOfSale",
                table: "Sales");

            migrationBuilder.DropColumn(
                name: "IsDiscount",
                table: "Accounts");
        }
    }
}
