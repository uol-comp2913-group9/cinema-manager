using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SEPCw1.Models;
using Syncfusion.Pdf;
using Syncfusion.Pdf.Graphics;
using Syncfusion.Drawing;
using System.IO;
using Syncfusion.Pdf.Barcode;
using System.Net.Mail;
using System.Net;
using System.Net.Mime;

namespace SEPCw1.Services
{
    public class BookingService
    {
        private readonly CinemaContext _context;
        private readonly MovieService _movieService;
        private readonly AccountService _accountService;

        public Showing FindShowing(long showingID)
        {
            foreach (var showing in _context.Showings)
                if (showing.Id == showingID)
                    return showing;

            throw new KeyNotFoundException($"Showing with ID: {showingID} was not found in database.");
        }

        public BookingService(CinemaContext context, MovieService movieService, AccountService accountService)
        {
            _context = context;
            _movieService = movieService;
            _accountService = accountService;
        }

        public async Task<Dictionary<Sale, uint>> GetAllSalesAsync(DateTime? since)
        {
            if (since == null)
                since = DateTime.MinValue;

            Dictionary<Sale, uint> saleDict = new();
            foreach (var sale in _context.Sales)
                if (sale.TimeOfSale >= since)
                    saleDict.Add(sale, await GetPriceofBookingAsync(sale.BookingId));

            return saleDict;
        }

        public class MovieAndGross
        {
            public Movie Movie { get; set; }
            public uint Gross { get; set; }

            public MovieAndGross(Movie movie, uint gross)
            {
                Movie = movie;
                Gross = gross;
            }
        }

        /// <summary>
        /// Gets a sorted list of the top n grossing movies
        /// </summary>
        /// <param name="numToReturn">is n</param>
        /// <returns>A sorted list of movies along with how much they made</returns>
        public async Task<List<MovieAndGross>> GetSpecificSalesAsync(int numToReturn)
        {
            if (numToReturn != 10)
                numToReturn = _context.Movies.Count();

            List<MovieAndGross> grossingMovies = new();
            foreach (var movie in _context.Movies)
            {
                MovieAndGross mvg = new(movie, 0);
                foreach (var showing in movie.Showings)
                    foreach (var booking in showing.Bookings)
                        foreach (var sale in booking.Sale)
                        {
                            if (sale.TimeOfSale >= DateTime.Now.AddDays(-7))
                                mvg.Gross += await GetPriceofBookingAsync(sale.BookingId);
                        }
            }

            return grossingMovies.OrderBy(x => x.Gross).ToList().GetRange(0, numToReturn);
        }

        internal async Task<List<Booking>> GetMyBookings(long accountID)
        {
            List<Booking> returnBookings = new List<Booking>();
            foreach (Booking booking in await _context.Bookings.ToListAsync())
            {
                if(booking.AccountId == accountID){
                    returnBookings.Add(booking);
                }
            }
            return returnBookings;
        }

        public async Task AddSaleAsync(List<long> bookingIDs, DateTime timeOfSale)
        {
            foreach (var bookingID in bookingIDs)
            {
                Sale sale = new();
                sale.BookingId = bookingID;
                sale.TimeOfSale = timeOfSale;

                await _context.Sales.AddAsync(sale);
            }

            await _context.SaveChangesAsync();
        }

        public async Task<List<Booking>> MakeBookingAsync(long showingID, long accountID, List<long> seats)
        {
            Showing theShow = FindShowing(showingID);

            List<Booking> newBookings = new();
            foreach (var seatID in seats)
            {
                if (!await SeatIsAvailableAsync(seatID, showingID))
                    continue;

                Booking newBooking = new();
                newBooking.ShowingId = theShow.Id;
                newBooking.AccountId = accountID;
                newBooking.SeatId = seatID;

                newBookings.Add(newBooking);
            }
            
            await _context.Bookings.AddRangeAsync(newBookings);
            await _context.SaveChangesAsync();
            await SendEmail(accountID,newBookings);
            return newBookings;
        }

        private async Task SendEmail(long accountID, List<Booking> newBookings)
        {
            var smtpClient = new SmtpClient("smtp.gmail.com")
            {
                Port = 587,
                Credentials = new NetworkCredential("woodhousepicturehouse@gmail.com", "woodhouse123"),
                EnableSsl = true,
            };
            
            Account acc = await _accountService.GetAccount(accountID);

            Showing showing =  FindShowing(newBookings[0].ShowingId);
            Movie moviev =  await _movieService.GetMovieAsync(showing.MovieId);
            var mailMessage = new MailMessage
            {
                From = new MailAddress("woodhousepicturehouse@gmail.com"),
                Subject = "Your WoodHouse PictureHouse tickets!",
                Body = $"<h1>Hello</h1><p>Thank you for your purchase!</p><p>Your tickets for {moviev.MovieName } {showing.TimeOfShowing.ToString("MMMM d, y, HH:mm")} are attached below.</p>",
                IsBodyHtml = true,
            };
            mailMessage.To.Add(acc.Username);

            foreach (Booking booking in newBookings)
            {
                Showing show =  FindShowing(booking.ShowingId);
                Movie mv =  await _movieService.GetMovieAsync(show.MovieId);
                MemoryStream fsr = GetTicketPDFAsync(booking.Id,mv.MovieName,show.TimeOfShowing,booking.SeatId);

                ContentType ct = new ContentType(MediaTypeNames.Text.Plain);
                Attachment attach = new Attachment(fsr, ct);
                attach.ContentDisposition.FileName = "Ticket"+booking.Id+".pdf";

                mailMessage.Attachments.Add(attach);
            }
            

            smtpClient.Send(mailMessage);
        }

        public MemoryStream GetTicketPDFAsync(long id, string movieName, DateTime timeOfShowing, long seatId)
        {
            //Create a new PDF document
            PdfDocument document = new();

            //Add a page to the document
            PdfPage page = document.Pages.Add();

            //Create PDF graphics for the page
            PdfGraphics graphics = page.Graphics;

            //Set the standard font
            PdfFont font = new PdfStandardFont(PdfFontFamily.Helvetica, 20);
            PdfFont font2 = new PdfStandardFont(PdfFontFamily.Helvetica, 15);

            PdfQRBarcode barcode = new PdfQRBarcode();

            //Set Error Correction Level

            barcode.ErrorCorrectionLevel = PdfErrorCorrectionLevel.High;

            //Set XDimension

            barcode.XDimension = 3;

            barcode.Text = "Validation for ticket " + id + " seat id " + seatId;


            //Draw the text
            graphics.DrawString("WoodHouse Picture House", font, PdfBrushes.Black, new PointF(0, 0));
            graphics.DrawString("Thank you for your purchase!", font2, PdfBrushes.Black, new PointF(0, 30));
            graphics.DrawString("Ticket #"+id, font2, PdfBrushes.Black, new PointF(0, 100));
            graphics.DrawString(movieName, font2, PdfBrushes.Black, new PointF(0, 130));
            graphics.DrawString(timeOfShowing.ToString("MMMM d, y, HH:mm"), font2, PdfBrushes.Black, new PointF(0, 160));
            graphics.DrawString("Seat " + seatId, font2, PdfBrushes.Black, new PointF(0, 190));

            barcode.Draw(page, new PointF(400, 100));


            //Saving the PDF to the MemoryStream
            MemoryStream stream = new();

            document.Save(stream);

            //Set the position as '0'.
            stream.Position = 0;

            //Download the PDF document in the browser
            return stream;
        }

        public async Task<uint> GetPriceofBookingAsync(long bookingID)
        {
            Booking b = await _context.Bookings.FindAsync(bookingID);
            uint bookingCost = b.Showing.Price;
            if (b.Seat.IsVIP)
                bookingCost = Convert.ToUInt32(bookingCost * b.Showing.LuxuryMultiplier);
            if (b.Account.IsDiscount)
                bookingCost = Convert.ToUInt32(bookingCost * b.Showing.DiscountMultiplier);

            return bookingCost;
        }

        /// <summary>
        /// Gets the price of the booking
        /// </summary>
        /// <param name="bookingIDs">A list of Booking IDs</param>
        /// <returns>An int representation of the price in pence</returns>
        public async Task<uint> GetBookingsPriceAsync(List<long> bookingIDs)
        {
            uint price = 0;
            foreach (var bookingID in bookingIDs)
                price += await GetPriceofBookingAsync(bookingID);

            return price;
        }

        public async Task<List<Seat>> GetAvailableSeatsAsync(long showingID)
        {
            List<Showing> allShowings = await _context.Showings.ToListAsync();
            Showing theShow = null;

            // finding the right showing 
            foreach (var show in allShowings)
            {
                if (show.Id == showingID)
                    theShow = show;
            }

            if (theShow == null)
                throw new KeyNotFoundException($"Showing with ID: {showingID} was not found in database.");

            List<Booking> allBookings = await _context.Bookings.ToListAsync();
            List<Seat> allSeats = await _context.Seats.ToListAsync();
            List<Seat> availableSeats = new();
            List<Seat> showSeats = new();
            List<Seat> bookedSeats = new();

            //finding all seats in the showing theShow / in that Screen
            foreach (var seat in allSeats)
            {
                if (seat.ScreenId == theShow.ScreenId)
                    showSeats.Add(seat);
            }

            //finding all booked seats in the showing theShow
            foreach (var booking in allBookings)
            {
                if (booking.ShowingId == theShow.Id)
                    bookedSeats.Add(booking.Seat);
            }

            availableSeats = showSeats.Except(bookedSeats).ToList();

            return availableSeats;
        }

        public async Task<List<Seat>> GetBookedSeatsAsync(long showingID)
        {
            List<Showing> allShowings = await _context.Showings.ToListAsync();
            Showing theShow = null;

            // finding the right showing 
            foreach (var show in allShowings)
            {
                if (show.Id == showingID)
                    theShow = show;
            }

            if (theShow == null)
                throw new KeyNotFoundException($"Showing with ID: {showingID} was not found in database.");

            List<Booking> allBookings = await _context.Bookings.ToListAsync();
            List<Seat> bookedSeats = new();

            //finding all booked seats in the showing theShow
            foreach (var booking in allBookings)
            {
                if (booking.ShowingId == theShow.Id)
                    bookedSeats.Add(booking.Seat);
            }

            return bookedSeats;
        }

        public async Task<bool> SeatIsAvailableAsync(long seatID, long showingID)
        {
            List<Seat> availableSeats = await GetAvailableSeatsAsync(showingID);

            foreach (var seat in availableSeats)
                if (seat.Id == seatID)
                    return true;

            return false;
        }

        private bool BookingExists(long id)
            => _context.Bookings.Any(e => e.Id == id);

        private bool SaleExists(long saleId)
            => _context.Sales.Any(e => e.SaleId == saleId);

        public async Task<Booking> GetBookingAsync(long id)
            => await _context.Bookings.FindAsync(id);

    }

}
