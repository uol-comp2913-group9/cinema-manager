using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SEPCw1.Models;

namespace SEPCw1.Services
{
    public class MovieService
    {

        private readonly CinemaContext _context;
        private readonly IMDBService _imdb;
        public MovieService(CinemaContext context, IMDBService service)
        {
            _context = context;
            _imdb = service;
        }

        public async Task DeleteMovieAsync(string id)
        {
            var movie = await _context.Movies.FindAsync(id);
            if (movie == null)
                throw new KeyNotFoundException($"Movie by id {id} was not found in database.");

            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
        }

        public async Task<Movie> AddMovieAsync(string movieID, Certificate cert, int price, string posterLink)
        {
            Movie movie = new();
            movie.Certificate = cert;
            movie.MovieID = movieID;
            movie.TicketPrice = price;
            movie.PosterLink = posterLink;
            _context.Movies.Add(movie);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                //From the original generated code, if this statement is true, it returned Conflict result
                if (MovieExists(movie.MovieID))
                    throw new Exception("Conflict");
                else throw;
            }

            return movie;
        }

        public async Task AddMoviePoster(string movieID)
        {
            Movie movie = await GetMovieAsync(movieID);
            string posterLink = null;
            if (MovieExists(movieID))
            {
                movie.PosterLink = posterLink;
            }
            //_context.Movies.Update(movie);
            await _context.SaveChangesAsync();
        }

        public async Task AddScreensAsync()
        {
            for (int i = 1; i <= 4; i++)
            {
                Screen screen = new();
                screen.Id = i;
                screen.SeatCount = 70;
                _context.Screens.Add(screen);
            }

            await _context.SaveChangesAsync();

        }

        public async Task AddSeatsAsync()
        {
            for (int j = 1; j <= 4; j++)
            {
                for (int i = 1; i <= 70; i++)
                {
                    Seat seat = new();
                    seat.ScreenId = j;
                    seat.Id = j * 1000 + i; // seatId format: Screen number*1000 + Seat number, ex: 4056 - Screen 4, Seat 56
                    seat.IsVIP = false;
                    if (i <= 30)
                        seat.IsVIP = true;

                    _context.Seats.Add(seat);
                }
            }
            await _context.SaveChangesAsync();
        }

        public async Task AddShowingsAsync()
        {
            Random random = new();
            for (int i = 1; i <= 50; i++)
            {
                Showing showing = new();
                showing.Id = i;
                List<Movie> movies = await GetMoviesAsync();
                List<Screen> screens = await _context.Screens.ToListAsync();
                int indexMovie = random.Next(movies.Count);
                showing.Movie = movies[indexMovie];
                int indexScreen = random.Next(screens.Count);
                showing.Screen = screens[indexScreen];
                showing.Price = 40;

                if (i % 2 == 0 && i % 3 != 0)
                    showing.TimeOfShowing = new DateTime(2021, 04, 1, 16, 00, 00);
                else if (i % 3 == 0 && i % 2 != 0)
                    showing.TimeOfShowing = new DateTime(2021, 04, 1, 19, 00, 00);
                else
                    showing.TimeOfShowing = new DateTime(2021, 04, 1, 21, 30, 00);

                _context.Showings.Add(showing);
            }

            await _context.SaveChangesAsync();
        }

        public async Task UpdateMovieTicketPrice(int price, string id)
        {
            var movie = await _context.Movies.FindAsync(id);
            if (movie == null)
                throw new KeyNotFoundException($"Movie with ID: {id} was not found in database.");

            movie.TicketPrice = price;
            await _context.SaveChangesAsync();
        }

        public async Task AddMovieSection(SectionType section, string id)
        {
            var movie = await _context.Movies.FindAsync(id);
            if (movie == null)
                throw new KeyNotFoundException($"Movie with ID: {id} was not found in database.");

            List<SectionMovie> sections = await _context.SectionMovies.ToListAsync();

            foreach (var sec in sections)
            {
                if (sec.MovieId == id)
                    sec.SectionType = section;
            }
        }

        public async Task AddMovieCertificate(Certificate cert, string id)
        {
            var movie = await _context.Movies.FindAsync(id);
            if (movie == null)
                throw new KeyNotFoundException($"Movie with ID: {id} was not found in database.");

            movie.Certificate = cert;
            await _context.SaveChangesAsync();
        }


        public async Task<List<Tuple<string,string>>> GetMoviesByNameAsync(string name)
        {
            List<Movie> movies = await GetMoviesAsync();
            List<Tuple<string,string>> ret = new List<Tuple<string, string>>();

            foreach (Movie mv in movies)
            {
                if(mv.MovieName.ToLower().Contains(name)){
                    ret.Add(new Tuple<string, string>(mv.MovieName,mv.MovieID));
                }
            }
            return ret;
        }

        public async Task<List<Movie>> GetMoviesByNameAsyncOld(string name)
        {
            List<string> movieIDs = (await _imdb.SearchMoviesByNameAsync(name, 10));
            List<Movie> movies = new();

            foreach (var movieID in movieIDs)
            {
                var movie = await _context.Movies.FindAsync(movieID);
                if (movie != null)
                    movies.Add(movie);
            }

            return movies;
        }

        public async Task<List<Movie>> GetMoviesByCastAsync(string cast)
        {
            List<Movie> allMovies = await GetMoviesAsync();
            List<Movie> movies = new();

            foreach (var movie in allMovies)
            {
                if (movie == null)
                    allMovies.Remove(movie);
            }

            foreach (var movie in allMovies)
            {
                List<String> actors = await _imdb.GetMovieCastAsync(movie.MovieID);

                foreach (var actor in actors)
                {
                    if (actor == null)
                        actors.Remove(actor);
                }

                foreach (var name in actors)
                {
                    if (name.Contains(cast))
                        movies.Add(movie);
                }
            }

            return movies;
        }

        Random rand = new Random();

        internal async Task<List<Movie>> GetTrendingFilms()
        {
            List<Movie> returnMovies = new List<Movie>();
            List<Movie> allShowings = await  GetMoviesAsync();
            for (int i = 0; i < 12; i++)
            {
                returnMovies.Add(allShowings.ElementAtOrDefault(rand.Next(0, allShowings.Count)));
            }
            return returnMovies;

        }

        internal async Task<List<Movie>> GetComingSoon()
        {
            List<Movie> returnMovies = new List<Movie>();
            List<Movie> allShowings = await  GetMoviesAsync();
            for (int i = 0; i < 12; i++)
            {
                returnMovies.Add(allShowings.ElementAtOrDefault(rand.Next(0, allShowings.Count)));
            }
            return returnMovies;
        }

        internal async Task<List<Movie>> GetNewReleases()
        {
            List<Movie> returnMovies = new List<Movie>();
            List<Movie> allShowings = await GetMoviesAsync();
            for (int i = 0; i < 12; i++)
            {
                returnMovies.Add(allShowings.ElementAtOrDefault(rand.Next(0, allShowings.Count)));
            }
            return returnMovies;
        }

        internal async Task<List<Movie>> GetTodaysFilms()
        {
            List<Movie> returnMovies = new List<Movie>();
            List<Movie> allShowings = await  GetMoviesAsync();
            for (int i = 0; i < 12; i++)
            {
                returnMovies.Add(allShowings.ElementAtOrDefault(rand.Next(0, allShowings.Count)));
            }
            return returnMovies;
        }

        public async Task<List<Seat>> GetAvailableSeatsAsync(long showingID)
        {
            List<Showing> allShowings = await _context.Showings.ToListAsync();
            Showing theShow = null;

            // finding the right showing 
            foreach (var show in allShowings)
            {
                if (show.Id == showingID)
                    theShow = show;
            }

            if (theShow == null)
                throw new KeyNotFoundException($"Showing with ID: {showingID} was not found in database.");

            List<Booking> allBookings = await _context.Bookings.ToListAsync();
            List<Seat> allSeats = await _context.Seats.ToListAsync();
            List<Seat> availableSeats = new();
            List<Seat> showSeats = new();
            List<Seat> bookedSeats = new();

            //finding all seats in the showing theShow / in that Screen
            foreach (var seat in allSeats)
            {
                if (seat.ScreenId == theShow.ScreenId)
                    showSeats.Add(seat);
            }

            //finding all booked seats in the showing theShow
            foreach (var booking in allBookings)
            {
                if (booking.ShowingId == theShow.Id)
                    bookedSeats.Add(booking.Seat);
            }

            availableSeats = showSeats.Except(bookedSeats).ToList();

            return availableSeats;
        }

        public async Task<List<Seat>> GetBookedSeatsAsync(long showingID)
        {
            List<Showing> allShowings = await _context.Showings.ToListAsync();
            Showing theShow = null;

            // finding the right showing 
            foreach (var show in allShowings)
            {
                if (show.Id == showingID)
                    theShow = show;
            }

            if (theShow == null)
                throw new KeyNotFoundException($"Showing with ID: {showingID} was not found in database.");

            List<Booking> allBookings = await _context.Bookings.ToListAsync();
            List<Seat> bookedSeats = new();

            //finding all booked seats in the showing theShow
            foreach (var booking in allBookings)
            {
                if (booking.ShowingId == theShow.Id)
                    bookedSeats.Add(booking.Seat);
            }

            return bookedSeats;
        }

        public async Task<List<Showing>> GetShowTimesAsync(string movieID)
        {
            var movie = await _context.Movies.FindAsync(movieID);
            if (movie == null)
                throw new KeyNotFoundException($"Movie with ID: {movieID} was not found in database.");

            List<Showing> allShowings = await _context.Showings.ToListAsync();
            List<Showing> showings = new();

            foreach (var show in allShowings)
            {
                if (show.MovieId == movieID)
                {
                    if (show != null)
                        showings.Add(show);
                }
            }

            return showings;
        }


        public async Task<List<Movie>> GetMoviesBySectionAsync(SectionType section)
        {
            List<SectionMovie> sections = await _context.SectionMovies.ToListAsync();
            List<Movie> movies = new();

            foreach (var sec in sections)
            {
                if (sec.SectionType == section)
                {
                    if (sec.Movie != null)
                        movies.Add(sec.Movie);
                }
            }

            return movies;
        }


        public async Task<List<Movie>> GetMoviesByGenreAsync(string genre)
        {
            List<Movie> allMovies = await GetMoviesAsync();
            List<Movie> movies = new();

            foreach (var movie in allMovies)
            {
                if (movie == null)
                    allMovies.Remove(movie);
            }

            foreach (var movie in allMovies)
            {
                String gotGenre = await _imdb.GetMovieGenre(movie.MovieID);

                if (gotGenre != null && gotGenre == genre)
                    movies.Add(movie);
            }

            return movies;
        }

        public async Task<Movie> GetMovieAsync(string id)
            => await _context.Movies.FindAsync(id);

        public async Task<string> GetMovieDescriptionAsync(Movie movie)
            => await _imdb.GetMovieDescriptionAsync(movie.MovieID);

        public async Task<List<Movie>> GetMoviesAsync(){
            List<Movie> allMovies = await _context.Movies.ToListAsync();

            foreach(Movie m in allMovies){
                if(m.MovieName == null){
                    m.MovieName = await _imdb.GetMovieTitleAsync(m);
                    await _context.SaveChangesAsync();
                }
            }

            return allMovies;
        }
            
        private bool MovieExists(string id)
           => _context.Movies.Any(e => e.MovieID == id);
    }
}
