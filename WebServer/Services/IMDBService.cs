using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SEPCw1.Models;

namespace SEPCw1.Services
{
    public class IMDBService
    {
        private class IMDBMovie
        {
            public string Title;
            public string Year;
            public string Rated;
            public string Released;
            public string Runtime;
            public string Genre;
            public string Director;
            public string Writer;
            public string Actors;
            public string Plot;
            public string Language;
            public string Country;
            public string Awards;
            public string Poster;
            public List<Dictionary<string, string>> Ratings;
            public string Metascore;
            public string imdbRating;
            public string imdbVotes;
            public string imdbID;
            public string Type;
            public string DVD;
            public string BoxOffice;
            public string Production;
            public string Website;
            public string Response;
        }

        private class OMDBSearchResults
        {
            public List<OMDBResult> Search;
            public string totalResults;
            public bool Response;
        }

        private class OMDBResult
        {
            public string Title;
            public string Year;
            public string imdbID;
            public string Type;
            public string Poster;
        }

        private const string _apiKey = "3ebea8f2";

        private static async Task<string> MakeJSONRequestAsync(string requestString)
        {
            string url = $"http://www.omdbapi.com/?apikey={_apiKey}&{requestString}";
            var response = new HttpClient().GetAsync(url);
            (await response).EnsureSuccessStatusCode();

            return await (await response).Content.ReadAsStringAsync();
        }

        public IMDBService() { }

        /// <summary>
        /// Gets the title of a movie from the IMDB database
        /// </summary>
        /// <param name="movie"></param>
        /// <returns></returns>
        public async Task<string> GetMovieTitleAsync(Movie movie)
            => await GetMovieTitleAsync(movie.MovieID);
        public async Task<string> GetMovieTitleAsync(string movieID)
        {
            var data = MakeJSONRequestAsync($"i={movieID}");
            return JsonConvert.DeserializeObject<IMDBMovie>(await data).Title;
        }

        /// <summary>
        /// Gets the description of a movie from the IMDB database
        /// </summary>
        /// <param name="movie"></param>
        /// <returns></returns>
        public async Task<string> GetMovieDescriptionAsync(Movie movie)
            => await GetMovieDescriptionAsync(movie.MovieID);
        public async Task<string> GetMovieDescriptionAsync(string movieID)
        {
            var data = MakeJSONRequestAsync($"i={movieID}&plot=full");
            return JsonConvert.DeserializeObject<IMDBMovie>(await data).Plot;
        }

        /// <summary>
        /// Searches the IMDB database for movies by a particular name
        /// </summary>
        /// <param name="term">The search term</param>
        /// <param name="numResults">The number of results to return</param>
        /// <returns>A list of IDs</returns>
        public async Task<List<string>> SearchMoviesByNameAsync(string term, int numResults)
        {
            OMDBSearchResults results = JsonConvert.DeserializeObject<OMDBSearchResults>(
                await MakeJSONRequestAsync($"s={term}")
            );


            int numResultsToGet = Math.Min(Convert.ToInt32(results.totalResults), numResults);
            int numPagesToGet = (numResultsToGet - 1) / 10 + 1; // This is equivalent to ceildividing by 10

            // This list contains lists of ten (possibly incomplete) ids
            // They are got asynchronously from the database
            List<Task<List<string>>> idTasks = new();
            // Loop until correct number of results are collected
            for (int page = 1; page < numPagesToGet; page++) idTasks.Add(Task.Run(async () =>
            {
                OMDBSearchResults data = JsonConvert.DeserializeObject<OMDBSearchResults>(
                    await MakeJSONRequestAsync($"s={term}&page={page}")
                );
                return data.Search.Select(x => x.Title).ToList();
            }));

            if(results.Search == null)
                return new List<string>();
            // We can now begin to await the tasks
            List<string> ids = new(results.Search.Select(x => x.Title));
            foreach (var tsk in idTasks)
                ids.AddRange(await tsk);

            return ids.GetRange(0, numResultsToGet);
        }

         /// <summary>
        /// Searches the IMDB database for movies by a particular name
        /// </summary>
        /// <param name="term">The search term</param>
        /// <param name="numResults">The number of results to return</param>
        /// <returns>A list of IDs</returns>
        public async Task<List<string>> SearchMoviesByNameAsyncOld(string term, int numResults)
        {
            OMDBSearchResults results = JsonConvert.DeserializeObject<OMDBSearchResults>(
                await MakeJSONRequestAsync($"s={term}")
            );


            int numResultsToGet = Math.Min(Convert.ToInt32(results.totalResults), numResults);
            int numPagesToGet = (numResultsToGet - 1) / 10 + 1; // This is equivalent to ceildividing by 10

            // This list contains lists of ten (possibly incomplete) ids
            // They are got asynchronously from the database
            List<Task<List<string>>> idTasks = new();
            // Loop until correct number of results are collected
            for (int page = 1; page < numPagesToGet; page++) idTasks.Add(Task.Run(async () =>
            {
                OMDBSearchResults data = JsonConvert.DeserializeObject<OMDBSearchResults>(
                    await MakeJSONRequestAsync($"s={term}&page={page}")
                );
                return data.Search.Select(x => x.imdbID).ToList();
            }));

            if(results.Search == null)
                return new List<string>();
            // We can now begin to await the tasks
            List<string> ids = new(results.Search.Select(x => x.imdbID));
            foreach (var tsk in idTasks)
                ids.AddRange(await tsk);

            return ids.GetRange(0, numResultsToGet);
        }

        public async Task<string> GetMovieDirectorNameAsync(string movieID)
        {
            var data = MakeJSONRequestAsync($"i={movieID}");
            return JsonConvert.DeserializeObject<IMDBMovie>(await data).Director;
        }

        public async Task<List<string>> GetMovieCastAsync(string movieID)
        {
            var data = MakeJSONRequestAsync($"i={movieID}");
            return JsonConvert.DeserializeObject<IMDBMovie>(await data).Actors.Split(',', 9999).ToList();
        }

        public async Task<string> GetMovieGenre(string movieID)
        {
            var data = MakeJSONRequestAsync($"i={movieID}");
            return JsonConvert.DeserializeObject<IMDBMovie>(await data).Genre;
        }
    }
}
