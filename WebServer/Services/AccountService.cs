using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SEPCw1.Models;

namespace SEPCw1.Services
{
    public class AccountService
    {

        private readonly CinemaContext _context;
        public AccountService(CinemaContext context)
        {
            _context = context;
        }

        public async Task PutAccount(long id, Account account)
        {
            if (id != account.Id)
                throw new ArgumentException($"id parameter is not equal to account.Id");

            _context.Entry(account).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AccountExists(id))
                    throw new KeyNotFoundException($"Account by id {id} does not exist in context");
                else throw;
            }
        }

        public async Task<Account> Register(string username, string password, string name, string surname, int age)
        {
            // validation
             if (string.IsNullOrEmpty(username))
                throw new ArgumentException("Username is required");

            if (string.IsNullOrWhiteSpace(password))
                throw new ArgumentException("Password is required");

            if (AccountExistsByUsername(username))
                throw new ArgumentException("User by the username \"" + username + "\" already exists");

            Tuple<string, string> hashedAndSaltedPassword = CreatePasswordHash(password);

            Account newAccount = new Account();
            newAccount.Username = username;
            newAccount.PasswordHash = hashedAndSaltedPassword.Item1;
            newAccount.PasswordSalt = hashedAndSaltedPassword.Item2;
            newAccount.Role = "Customer";
            newAccount.Age = age;
            newAccount.Name = name;
            newAccount.Surname = surname;
            //Takes first 8 bytes of unique identifier and converts it to long to store to database.
            newAccount.Id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);

            await AddAccount(newAccount);
            return newAccount;
        }

        /// <summary>
        /// Takes a plaintext password and hashes it.
        /// </summary>
        /// <returns>
        /// Tuple of two base64 strings: hashed password and password salt.
        /// </returns>
        private static Tuple<string,string> CreatePasswordHash(string password)
        {
            if (password == null) throw new ArgumentNullException();
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Password cannot be empty or whitespace");

            byte[] passwordHash, passwordSalt;
            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
            
            return new Tuple<string,string>(Convert.ToBase64String(passwordHash),Convert.ToBase64String(passwordSalt));
        }

        private static bool VerifyPasswordHash(string password, byte[] storedHash, byte[] storedSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");
            if (storedHash.Length != 64) throw new ArgumentException("Invalid length of password hash (64 bytes expected).", "passwordHash");
            if (storedSalt.Length != 128) throw new ArgumentException("Invalid length of password salt (128 bytes expected).", "passwordHash");

            using (var hmac = new System.Security.Cryptography.HMACSHA512(storedSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != storedHash[i]) return false;
                }
            }

            return true;
        }

        public async Task<Account> Authenticate(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                return null;


            Account user = null;
            foreach(Account acc in await GetAccounts()){
                if(acc.Username == username)
                {
                    user=acc;
                    break;
                }
            }

            // check if username exists
            if (user == null)
                return null;

            // check if password is correct
            if (!VerifyPasswordHash(password, Convert.FromBase64String(user.PasswordHash), Convert.FromBase64String(user.PasswordSalt)))
                return null;

            return user;
        }

        public async Task<List<Booking>> ViewMyBookingsAsync(long accountID)
        {   
            List<Booking> bookings = new();
            List<Booking> allBookings = await _context.Bookings.ToListAsync();

            foreach(var booking in allBookings) {
                if (booking.AccountId == accountID) {
                    if (booking != null)
                        bookings.Add(booking);
                }
            }
            return bookings;
        }

        public async Task AddAccount(Account account)
        {
            _context.Accounts.Add(account);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAccount(long id)
        {
            var account = await _context.Accounts.FindAsync(id);
            if (account == null)
                throw new KeyNotFoundException($"Account by id {id} was not found in database.");

            _context.Accounts.Remove(account);
            await _context.SaveChangesAsync();
        }

        public async Task<Account> GetAccount(long id)
        {
            var account = await _context.Accounts.FindAsync(id);

            if (account == null)
                throw new KeyNotFoundException($"Account by id {id} does not exist in context");
            return account;
        }

        public async Task<List<Account>> GetAccounts()
            => await _context.Accounts.ToListAsync();
        public bool AccountExists(long id)
            => _context.Accounts.Any(e => e.Id == id);

         private bool AccountExistsByUsername(string username)
            => _context.Accounts.Any(e => e.Username == username);

    }
}
