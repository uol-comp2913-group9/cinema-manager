using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SEPCw1.Models;
using Stripe;
using Stripe.Checkout;

namespace SEPCw1.Services
{
    public class PaymentsService
    {

        private readonly CinemaContext _context;
        public PaymentsService(CinemaContext context)
        {
            _context = context;
            StripeConfiguration.ApiKey = "sk_test_51Ia52XG1jikiNj1gsoLPwvsLzdIxyGFUyK1s2pJPCZjAX7rcGIMNtDcVcJvLAHBto9vmr2dYMJRbUihAi6cJcM2a00OKxynEKO";
        }

        public string CreateCharge(PaymentRequestModel model, uint price, float discountMultiplier, float luxuryMultiplier)
        {
            var options = new SessionCreateOptions
            {
                SuccessUrl = "https://localhost:5001/paymentsuccessful",
                CancelUrl = "https://localhost:5001",
                PaymentMethodTypes = new List<string>
            {
                "card",
            },
                LineItems = new List<SessionLineItemOptions>
            {
                
            },
                Mode = "payment",
            };

            if(model.AdultSeats > 0){
                options.LineItems.Add(new SessionLineItemOptions {
                        Name = "Adult ticket(s)",
                        Amount = price,
                        Currency = "GBP",
                        Quantity = model.AdultSeats
                });
            }

            if(model.ChildSeats > 0){
                options.LineItems.Add(new SessionLineItemOptions {
                        Name = "Children ticket(s)",
                        Amount = (int)(price*discountMultiplier),
                        Currency = "GBP",
                        Quantity = model.ChildSeats
                });
            }

            if(model.ChildSeats > 0){

                options.LineItems.Add(new SessionLineItemOptions {
                        Name = "Senior ticket(s)",
                        Amount = (int)(price*discountMultiplier),
                        Currency = "GBP",
                        Quantity = model.SeniorSeats
                });
            }

            long totalPrice = 0;
            totalPrice += model.AdultSeats * price;
            totalPrice += (int)(model.ChildSeats * price  *  discountMultiplier);
            totalPrice += (int)(model.SeniorSeats * price * discountMultiplier);

            if(model.VipSeats > 0){
                int averagePrice = (int)(totalPrice/(model.AdultSeats+model.ChildSeats+model.SeniorSeats));
                int vipAddition = (int)(averagePrice*luxuryMultiplier - averagePrice);
                    options.LineItems.Add(new SessionLineItemOptions {
                        Name = "VIP seats charge",
                        Amount = vipAddition,
                        Currency = "GBP",
                        Quantity = model.VipSeats
                });
            }


            var service = new SessionService();
            Session s = service.Create(options);
            Console.WriteLine(s.Id);
            return s.Id;
        }

        internal async Task AcceptCashPayment(long refCode)
        {
            CashPayment p = await GetCashPaymentAsync(refCode);
            p.Paid = true;
            await _context.SaveChangesAsync();
        }

        public async Task<string> CreateCashCharge(PaymentRequestModel model, uint price, float discountMultiplier, float luxuryMultiplier, long accountID)
        {
            CashPayment payment = new CashPayment();
            payment.AccountId = accountID;
            payment.AdultSeats = model.AdultSeats;
            payment.ChildSeats = model.ChildSeats;
            payment.SeniorSeats = model.SeniorSeats;
            payment.VipSeats = model.VipSeats;
            payment.Paid = false;
            Random rand = new Random();
            payment.Id = rand.Next(100000,999999);
            payment.ShowingId = model.ShowingId;

            long totalPrice = 0;
            totalPrice += model.AdultSeats * price;
            totalPrice += (int)(model.ChildSeats * price  *  discountMultiplier);
            totalPrice += (int)(model.SeniorSeats * price * discountMultiplier);

            if(model.VipSeats > 0){
                int averagePrice = (int)(totalPrice/(model.AdultSeats+model.ChildSeats+model.SeniorSeats));
                int vipAddition = (int)(averagePrice*luxuryMultiplier - averagePrice);
                totalPrice += vipAddition*model.VipSeats;
            }

            payment.PricePaid = totalPrice;

            await _context.CashPayments.AddAsync(payment);
            await _context.SaveChangesAsync();

            return payment.Id.ToString();
        }

        public async Task<CashPayment> GetCashPaymentAsync(long id)
            => await _context.CashPayments.FindAsync(id);

    }
}
