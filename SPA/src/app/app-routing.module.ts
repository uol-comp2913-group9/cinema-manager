import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomepageComponent } from './homepage/homepage.component';
import { RegisterComponent } from './register/register.component';
import { MoviesComponent } from './movies/movies.component';
import { LoginformComponent } from './loginform/loginform.component';
import { StaffPageComponent } from './staff-page/staff-page.component';
import { PaymentSuccessfulComponent } from './payment-successful/payment-successful.component';
import { OwnerComponent } from './owner/owner.component';
import { MyTicketsComponent } from './my-tickets/my-tickets.component';

const routes: Routes = [
  { path: '', component: HomepageComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'movies/:id', component: MoviesComponent },
  { path: 'login', component: LoginformComponent },
  { path: 'staff', component: StaffPageComponent },
  { path: 'owner', component: OwnerComponent },
  { path: 'mytickets', component: MyTicketsComponent },
  { path: 'paymentsuccessful', component: PaymentSuccessfulComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
