import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-loginform',
  templateUrl: './loginform.component.html',
  styleUrls: ['./loginform.component.scss']
})
export class LoginformComponent implements OnInit {

  constructor(public http: HttpClient, private router: Router, private snackBar: MatSnackBar) { }

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  passwordFormControl = new FormControl('', [
    Validators.required,
  ]);

  ngOnInit(): void {
  }

  processLogin(){

    let username = this.emailFormControl.value;
    let password = this.passwordFormControl.value;
    this.http.post(`https://localhost:5001/api/account/authenticate`, { username, password }).subscribe(
    {
      next: succ => {
        let success : any = succ;
        localStorage.setItem('usertoken', success.token);
        localStorage.setItem('username', success.username);
        localStorage.setItem('role', success.role);
        console.log(succ)
        this.router.navigate(['/']);
      },
      error: error => {
        console.log(error);
        this.snackBar.open(error.error.message, '', {
          duration: 5000,
        });
      }
    })

  }
}
