import { HttpClient } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CashRefferenceDialogComponent } from '../cash-refference-dialog/cash-refference-dialog.component';

export interface DialogData {
  data: any;
}

@Component({
  selector: 'app-confirm-cash-payment-dialog',
  templateUrl: './confirm-cash-payment-dialog.component.html',
  styleUrls: ['./confirm-cash-payment-dialog.component.scss'],
})
export class ConfirmCashPaymentDialogComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<CashRefferenceDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public http: HttpClient
  ) {}

  ngOnInit(): void {}

  accept() {
    this.http
      .post('/api/payments/AcceptCashPayment/' + this.data.data.id, {})
      .subscribe(
        (result) => {
          console.log(result);
        },
        (error) => {
          console.error(error);
        }
      );
  }
}
