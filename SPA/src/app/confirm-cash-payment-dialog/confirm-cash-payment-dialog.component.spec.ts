import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmCashPaymentDialogComponent } from './confirm-cash-payment-dialog.component';

describe('ConfirmCashPaymentDialogComponent', () => {
  let component: ConfirmCashPaymentDialogComponent;
  let fixture: ComponentFixture<ConfirmCashPaymentDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfirmCashPaymentDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmCashPaymentDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
