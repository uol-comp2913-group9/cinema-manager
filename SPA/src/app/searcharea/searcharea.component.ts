import { HttpClient } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import {map, startWith} from 'rxjs/operators';

interface option{
  item1: string;
  item2: string;
}


@Component({
  selector: 'app-searcharea',
  templateUrl: './searcharea.component.html',
  styleUrls: ['./searcharea.component.scss']
})
export class SearchareaComponent implements OnInit {

  myControl = new FormControl();
  options: option[] = [];
  filteredOptions: Observable<option[]>;

  constructor(public http: HttpClient, private router: Router) {
    this.myControl.valueChanges.subscribe((d)=>{
      console.log(d);
    });
  }

  private _filter(value: string): option[] {
    const filterValue = value.toLowerCase();

    this.http.get<option[]>(`https://localhost:5001/api/movie/searchbyname/${filterValue}`)
      .subscribe(
        (result) => {
          console.log(result);
          this.options = result;
          // this._filter("");
        },
        (error) => {
          console.error(error);
        }
      );

    return this.options.filter(x => x);

  }

  ngOnInit(): void {
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
  }

  navigateTo(movieId:string){
    this.router.navigate(['/movies/'+movieId]);

  }

}
