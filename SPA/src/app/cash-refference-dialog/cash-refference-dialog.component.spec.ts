import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CashRefferenceDialogComponent } from './cash-refference-dialog.component';

describe('CashRefferenceDialogComponent', () => {
  let component: CashRefferenceDialogComponent;
  let fixture: ComponentFixture<CashRefferenceDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CashRefferenceDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CashRefferenceDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
