import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface DialogData {
  refferenceCode: string;
}

@Component({
  selector: 'app-cash-refference-dialog',
  templateUrl: './cash-refference-dialog.component.html',
  styleUrls: ['./cash-refference-dialog.component.scss']
})
export class CashRefferenceDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<CashRefferenceDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {

    }

  ngOnInit(): void {
  }

}
