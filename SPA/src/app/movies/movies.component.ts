import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { SeatPopupComponent } from '../seat-popup/seat-popup.component';

interface Genres {
  value: string;
  viewValue: string;
}

export class Showing{
  price: number;
  id: number;
  startTime: Date;
  endTime: Date;
  screen: number;
  luxuryMultiplier: number;
  discountMultiplier: number;
}

class ShowningGroup {
  dateString: string;
  date:Date;
  showings:Showing[];
}

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.scss']
})

export class MoviesComponent implements OnInit {

  constructor(public http: HttpClient, private route: ActivatedRoute, public dialog: MatDialog) { }

  allShowings:ShowningGroup[] = [];

  movieDetails:any ;
  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      let id  = params.get('id');

      this.http.get("https://localhost:5001/api/movie/GetMovie/"+ id).subscribe(result => {
        console.log(result)
        this.movieDetails = result;

        let showingsObject = this.movieDetails.showings;
        console.log();

        for(let i = 0; i < showingsObject.length; i++){
          console.log(showingsObject[i]);
          let dateString:string = showingsObject[i].Date.split(' ')[0];

          let newShowing: Showing = new Showing();
          newShowing.id = showingsObject[i].ID;
          newShowing.price = showingsObject[i].Price;
          newShowing.startTime = new Date(showingsObject[i].Date);
          newShowing.endTime = new Date(showingsObject[i].Date);
          newShowing.screen =  showingsObject[i].Screen;
          newShowing.luxuryMultiplier =  showingsObject[i].LuxuryMultiplier;
          newShowing.discountMultiplier =  showingsObject[i].DiscountMultiplier;

          //Add fake end time
          newShowing.endTime.setHours(newShowing.endTime.getHours() + 2);

          let added: boolean = false;
          for(let j = 0; j < this.allShowings.length; j++){
            if(this.allShowings[j].dateString == dateString){
              this.allShowings[j].showings.push(newShowing);
              added = true;
              break;
            }
          }

          if(!added){
            let newShowingGroup: ShowningGroup = new ShowningGroup();
            newShowingGroup.dateString = dateString;
            newShowingGroup.date = new Date(showingsObject[i].Date);
            newShowingGroup.showings = [];
            newShowingGroup.showings.push(newShowing);
            this.allShowings.push(newShowingGroup);
          }
        }
        console.log(this.allShowings);
      },
      error => { console.error(error); });
    });
  }

  getCertificatByEnum(cert:number){
    if(cert === 0) return "Universal (U)";
    else if (cert == 1) return "Parental Guidance Suggested (PG)";
    else if (cert == 2) return "Parental Guidance Cautioned (PG13)";
    else if (cert == 3) return "Rated for over 15 (R15)";
    else if (cert == 4) return "Rated for over 18 (R18)";
    else return "Unknown";
  }


  genres: Genres[] = [
    {value: 'Action -0', viewValue: 'Action'},
    {value: 'Adventure -1', viewValue: 'Adventure'},
    {value: 'Cartoon-2', viewValue: 'Cartoon'},
    {value: 'Comedy-3', viewValue: 'Comedy'},
    {value: 'Documentary-4', viewValue: 'Documentary'},
    {value: 'Drama-5', viewValue: 'Drama'},
    {value: 'Fantasy-6', viewValue: 'Fantasy'},
    {value: 'Romance-7', viewValue: 'Romance'},
    {value: 'Sci-fi-8', viewValue: 'Sci-fi'}
  ];

  images = [944, 1011, 984, 985, 986, 987, 988, 989, 990, 991, 992, 993, 994].map((n) => `https://picsum.photos/id/${n}/900/500`);

  openDialog(show:Showing) {
    const dialogRef = this.dialog.open(SeatPopupComponent, {
      data: {show: show, movieId: this.movieDetails.movieID, certifiacte:this.movieDetails.certificate}
    });
    console.log(show);
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
}
