import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {


  nameFormControl = new FormControl('', [Validators.required]);
  surFormControl = new FormControl('', [Validators.required]);
  emailFormControl = new FormControl('', [Validators.required]);
  ageFormControl = new FormControl('', [Validators.required]);
  passwordFormControl = new FormControl('', [Validators.required]);
  password2FormControl = new FormControl('', [Validators.required]);

  constructor(public http: HttpClient, private router: Router, private snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }

  register(){
    let name = this.nameFormControl.value;
    let surname = this.surFormControl.value;
    let email = this.emailFormControl.value;
    let age = this.ageFormControl.value;
    let password = this.passwordFormControl.value;
    let repeatPassword = this.password2FormControl.value;

    this.http.post(`https://localhost:5001/api/account/register`, { name, surname, email, age, password, repeatPassword }).subscribe(
    {
      next: succ => {
        let success : any = succ;
        localStorage.setItem('usertoken', success.token);
        localStorage.setItem('username', success.username);
        console.log(succ)
        this.router.navigate(['/']);
      },
      error: error => {
        console.log(error);
        this.snackBar.open(error.error.message, '', {
          duration: 5000,
        });
      }
    })
  }

}
