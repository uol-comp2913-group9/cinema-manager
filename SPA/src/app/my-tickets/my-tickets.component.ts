import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

interface MyBooking{
  movieName: string;
  seatId: number;
  screen: number;
  date: Date;
  id: number;
}

@Component({
  selector: 'app-my-tickets',
  templateUrl: './my-tickets.component.html',
  styleUrls: ['./my-tickets.component.scss']
})
export class MyTicketsComponent implements OnInit {

  displayedColumns: string[] = ['name', 'seat', 'screen', 'date', 'ticket'];
  dataSource = [];

  constructor(public http: HttpClient) {

    this.http.get<MyBooking[]>("https://localhost:5001/api/booking/GetMyTickets").subscribe(
    result => {console.log(result); this.dataSource = result },
    error => { console.error(error); });

  }

  ngOnInit(): void {
  }

  viewTicket(id:number){
    // let headers = new HttpHeaders();
    // headers = headers.set('Accept', 'application/pdf');
    // this.http.get("https://localhost:5001/api/booking/GetTicketPdf/"+id, { headers: headers, responseType: 'blob' }).subscribe(
    //   result => {console.log(result); },
    //   error => { console.error(error); });
    window.location.href = "https://localhost:5001/api/booking/GetTicketPdf/"+id;

  }

}
