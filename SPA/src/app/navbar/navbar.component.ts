import { listLazyRoutes } from '@angular/compiler/src/aot/lazy_routes';
import { Component, DoCheck, OnChanges, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit, DoCheck {

  constructor() { }
  ngDoCheck(): void {
    let username: any = localStorage.getItem("username");
  }

  getAccountName(): string {
    if(localStorage.getItem("username") === null){
      return "Account";
    } else {
      return localStorage.getItem("username");
    }
  }

  getRoleName(): string {
    if(localStorage.getItem("role") === null){
      return "Customer";
    } else {
      return localStorage.getItem("role");
    }
  }

  isLoggedIn(): boolean {
    if(localStorage.getItem("usertoken") === null){
      return false;
    } else {
      return true;
    }
  }

  logOut(){
    localStorage.removeItem("username");
    localStorage.removeItem("usertoken");
    localStorage.removeItem("role");
  }

  ngOnInit(): void {
  }

}
