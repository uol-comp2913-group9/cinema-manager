import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { InfoPopupComponent } from 'src/app/info-popup/info-popup.component';

interface Certificate {
  value: number;
  viewValue: string;
}

@Component({
  selector: 'app-add-movie',
  templateUrl: './add-movie.component.html',
  styleUrls: ['./add-movie.component.scss'],
})
export class AddMovieComponent implements OnInit {
  newMovieDetails = { movieID: '', certificate: 0, ticketprice: 0, posterLink:"" };
  certificates: Certificate[] = [
    { value: 0, viewValue: 'U' },
    { value: 1, viewValue: 'PG' },
    { value: 2, viewValue: '12' },
    { value: 3, viewValue: '15' },
    { value: 4, viewValue: '18' },
  ];

  constructor(public http: HttpClient, public notifyDialog: MatDialog) {}

  ngOnInit(): void {}

  newMovie() {
    this.http
      .post('/api/movie/addmovie', {
        MovieID: this.newMovieDetails.movieID,
        Certificate: this.newMovieDetails.certificate,
        TicketPrice: this.newMovieDetails.ticketprice,
        PosterLink: this.newMovieDetails.posterLink
      })
      .subscribe(
        (result) => {
          console.log(result);
          this.notifyDialog.open(InfoPopupComponent, {
            data: {
              message: result,
            },
          });
        },
        (error) => {
          console.error(error);
          this.notifyDialog.open(InfoPopupComponent, {
            data: {
              message: error,
            },
          });
        }
      );
  }
}
