import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmCashPaymentDialogComponent } from 'src/app/confirm-cash-payment-dialog/confirm-cash-payment-dialog.component';
import { InfoPopupComponent } from 'src/app/info-popup/info-popup.component';

@Component({
  selector: 'app-confirm-payment',
  templateUrl: './confirm-payment.component.html',
  styleUrls: ['./confirm-payment.component.scss']
})
export class ConfirmPaymentComponent implements OnInit {

  refCode = "";

  constructor(public http: HttpClient, public dialog: MatDialog) {}

  ngOnInit(): void {}

  newMovie() {
    console.log(this.refCode)
    this.http
      .get('/api/payments/GetCashPayment/' + this.refCode)
      .subscribe(
        (result) => {
          console.log(result);

          const dialogRef = this.dialog.open(ConfirmCashPaymentDialogComponent, {
            width: '500px',
            data: {data: result}
          });

        },
        (error) => {
          console.error(error);
        }
      );
  }

}
