import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SeatPopupComponent } from './seat-popup.component';

describe('SeatPopupComponent', () => {
  let component: SeatPopupComponent;
  let fixture: ComponentFixture<SeatPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SeatPopupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SeatPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
