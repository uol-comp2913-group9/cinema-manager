declare var Stripe: any;
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CashRefferenceDialogComponent } from '../cash-refference-dialog/cash-refference-dialog.component';
import { Showing } from '../movies/movies.component';

interface Food {
  value: string;
  viewValue: string;
}

interface DialogData{
  show:Showing;
  movieId:string;
  certifiacte: number;
}
@Component({
  selector: 'app-seat-popup',
  templateUrl: './seat-popup.component.html',
  styleUrls: ['./seat-popup.component.scss'],
})
export class SeatPopupComponent implements OnInit {
  foods: Food[] = [
    { value: 'steak-0', viewValue: '04:00 PM - 06:00 PM ' },
    { value: 'pizza-1', viewValue: '07:00 PM - 09:00 PM' },
    { value: 'tacos-2', viewValue: '09:00 PM - 11:00 PM' },
  ];

  bookedSeats: Boolean[] = [];
  checkedSeats: Boolean[] = [];
  date: Date;

  adultSeats = 0;
  childSeats = 0;
  seniorSeats = 0;
  numberVIP;

  showId = 0;
  availableSeats: any = {};

  totalPrice = 0;

  childrenDisabled = false;

  constructor(
    public http: HttpClient,
    public dialogRef: MatDialogRef<SeatPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public dialog: MatDialog
  ) {
    console.log(data);
    this.date = data.show.startTime;
    this.showId = data.show.id;

    if(this.data.certifiacte > 2){
      this.childrenDisabled = true;
    }

    for (let i = 0; i < 70; i++) {
      this.bookedSeats[i] = true;
      this.checkedSeats[i] = false;
    }

    this.http
      .get(
        'https://localhost:5001/api/booking/GetAvailableSeats/' +
          data.movieId +
          '/' +
          data.show.id
      )
      .subscribe((result) => {
        this.availableSeats = result;
        for (let i = 0; i < this.availableSeats.length; i++) {
          let id: number = this.availableSeats[i].id % 100;
          id--;
          this.bookedSeats[id] = false;
        }
      });
  }

  ngOnInit(): void {}

  proceedToPayment() {
    let Seats: number[] = [];
    for (let i = 0; i < this.availableSeats.length; i++) {
      if (this.checkedSeats[i] === true) {
        console.log(this.availableSeats[i]);
        Seats.push(this.availableSeats[i].id);
      }
    }



    let ShowingID = this.showId;
    this.http
      .post(`https://localhost:5001/api/booking/MakeBooking`, {
        ShowingID,
        Seats,
      })
      .subscribe({
        next: (succ) => {
            var stripe = Stripe('pk_test_51Ia52XG1jikiNj1gsowGDOQid3XetIcujIhffKJq3DVDIlj7QQsfoy10sQ7bt1YHe2Bx3djogPKh7XiDImpsCj590021X1C4BM');
            this.http.post("https://localhost:5001/api/payments/PayByCard", {
              AdultSeats:this.adultSeats,
              ChildSeats:this.childSeats,
              SeniorSeats: this.seniorSeats,
              VipSeats: this.numberVIP,
              ShowingId: this.showId

            }).subscribe(result => {
                console.log("Succ");
                console.log(result);
              },
              error => {
                console.log("err");
                console.log(error);
                stripe.redirectToCheckout({ sessionId: error.error.text  });
              });
        },
        error: (error) => {
          console.log(error);
        },
      });
  }

  proceedToCashPayment() {
    let Seats: number[] = [];
    for (let i = 0; i < this.availableSeats.length; i++) {
      if (this.checkedSeats[i] === true) {
        console.log(this.availableSeats[i]);
        Seats.push(this.availableSeats[i].id);
      }
    }



    let ShowingID = this.showId;
    this.http
      .post(`https://localhost:5001/api/booking/MakeBooking`, {
        ShowingID,
        Seats,
      })
      .subscribe({
        next: (succ) => {
            var stripe = Stripe('pk_test_51Ia52XG1jikiNj1gsowGDOQid3XetIcujIhffKJq3DVDIlj7QQsfoy10sQ7bt1YHe2Bx3djogPKh7XiDImpsCj590021X1C4BM');
            this.http.post("https://localhost:5001/api/payments/PayByCash", {
              AdultSeats:this.adultSeats,
              ChildSeats:this.childSeats,
              SeniorSeats: this.seniorSeats,
              VipSeats: this.numberVIP,
              ShowingId: this.showId

            }).subscribe(result => {
                console.log("Succ");
                console.log(result);

                  const dialogRef = this.dialog.open(CashRefferenceDialogComponent, {
                    width: '500px',
                    data: {refferenceCode: result}
                  });

              },
              error => {
                console.log("err");
                console.log(error);
              });
        },
        error: (error) => {
          console.log(error);
        },
      });
  }



  seatButtonClicked() {
    this.adultSeats = 0;
    this.childSeats = 0;
    this.seniorSeats = 0;

    this.numberVIP = 0;
    for (let i = 0; i < this.checkedSeats.length; i++) {

      if (this.checkedSeats[i] == true){
        this.adultSeats++;
        if(i < 30){
          this.numberVIP++;
        }
      }
    }
    this.updatePrice();
  }

  addChild() {
    if (this.adultSeats > 0) {
      this.childSeats++;
      this.adultSeats--;
      this.updatePrice();
    }
  }

  removeChild() {
    if (this.childSeats > 0) {
      this.childSeats--;
      this.adultSeats++;
      this.updatePrice();
    }
  }

  addSenior() {
    if (this.adultSeats > 0) {
      this.seniorSeats++;
      this.adultSeats--;
      this.updatePrice();
    }
  }

  removeSenior() {
    if (this.seniorSeats > 0) {
      this.seniorSeats--;
      this.adultSeats++;
      this.updatePrice();
    }
  }

  updatePrice(){
    this.totalPrice = 0;
    this.totalPrice += this.adultSeats * this.data.show.price;
    this.totalPrice += this.childSeats * this.data.show.price *  this.data.show.discountMultiplier;
    this.totalPrice += this.seniorSeats * this.data.show.price *  this.data.show.discountMultiplier;
    let averagePrice = this.totalPrice/(this.adultSeats+this.childSeats+this.seniorSeats);
    let vipAddition = averagePrice*this.data.show.luxuryMultiplier - averagePrice;
    this.totalPrice += vipAddition*this.numberVIP;
    if(!this.totalPrice)
      this.totalPrice = 0;
  }
}
