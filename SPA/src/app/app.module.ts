import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material-module';
import { LoginformComponent } from './loginform/loginform.component';
import { SearchareaComponent } from './searcharea/searcharea.component';
import { HomepageComponent } from './homepage/homepage.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegisterComponent } from './register/register.component';
import { MoviesComponent } from './movies/movies.component';
import { FooterComponent } from './footer/footer.component';
import { SeatPopupComponent } from './seat-popup/seat-popup.component';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { StaffPageComponent } from './staff-page/staff-page.component';
import { InfoPopupComponent } from './info-popup/info-popup.component';
import { PaymentSuccessfulComponent } from './payment-successful/payment-successful.component';
import { AddMovieComponent } from './staff-page/add-movie/add-movie.component';
import { OwnerComponent } from './owner/owner.component';
import { JwtInterceptor } from './jwt.interceptor';
import { CashRefferenceDialogComponent } from './cash-refference-dialog/cash-refference-dialog.component';
import { ConfirmCashPaymentDialogComponent } from './confirm-cash-payment-dialog/confirm-cash-payment-dialog.component';
import { ConfirmPaymentComponent } from './staff-page/confirm-payment/confirm-payment.component';
import { MyTicketsComponent } from './my-tickets/my-tickets.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginformComponent,
    SearchareaComponent,
    HomepageComponent,
    RegisterComponent,
    MoviesComponent,
    FooterComponent,
    SeatPopupComponent,
    StaffPageComponent,
    InfoPopupComponent,
    PaymentSuccessfulComponent,
    AddMovieComponent,
    OwnerComponent,
    CashRefferenceDialogComponent,
    ConfirmCashPaymentDialogComponent,
    ConfirmPaymentComponent,
    MyTicketsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    BrowserAnimationsModule,
    NgbModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
