# Building and running the website

## Backend Server

To build web server, you are required to have ASP.NET core 5 dependency. This can be downloaded [here.](https://dotnet.microsoft.com/download/dotnet/5.0)

Open terminal in WebServer folder and run the server by using `dotnet restore` followed by  `dotnet run`



## Frontend Server

To build frontend server, you are required to have angular CLI installed. Instructions can be found [here.](https://angular.io/cli)

Open terminal in SPA folder and run the server by using `npm install` followed by `ng serve`



## Accessing Website

After both servers are running, you may access the website by going to https://localhost:5001

